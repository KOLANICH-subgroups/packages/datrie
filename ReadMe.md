python3-datrie wheel packages
-----------------------------
![GitLab Build Status](https://gitlab.com/KOLANICH-subgroups/packages/datrie/badges/master/pipeline.svg)
[wheel](https://gitlab.com/KOLANICH-subgroups/packages/datrie/-/jobs/artifacts/master/raw/wheels/datrie-0.CI_python-py3-none-any.whl?job=build)
![GitLab Coverage](https://gitlab.com/KOLANICH-subgroups/packages/datrie/badges/master/coverage.svg)

Maintainers of [python datrie package](https://github.com/pytries/datrie) don't react rapidly enough on issues and even severe bugs. That repo is essentially unmaintained. So I had to create an own pipeline.
